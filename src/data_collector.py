
from collections import deque
from config import sep_sign
class collector:
    """class for reading upcoming values with number of input_number from defined path"""
    def __init__(self,path,input_number):
        self.path=path
        self.input_number=input_number
        self.last=None
        self.last_data=[]
    def get_latest(self):
        if self.path != None:
            tmp=deque(open(self.path), 1)
            self.last=tmp[0]
            return
        else:
            tmp="0"
            for i in range(0,self.input_number):
                tmp=tmp+",0"
            self.last=tmp
    def splitandinterp(self):
        if self.last != None:
            tmp=self.last.split(sep_sign,self.input_number)
            for i in range(0,len(tmp)):
                tmp[i]=int(tmp[i])
            
            self.last_data=tmp
    def fast_get(self):
        self.get_latest()
        self.splitandinterp()
        return self.last_data
        
