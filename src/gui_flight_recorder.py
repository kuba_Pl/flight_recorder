
from ttk import *
from Tkinter import *
import tkFileDialog
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import tkMessageBox

from config import *
from data_save import basic_save
from data_collector import collector
from gui_archives import Arch_manager

class Recorder(Frame):
    """gui class"""
    def __init__(self, parent):
        Frame.__init__(self, parent)  
        self.parent = parent
        self.collectors=[]
        tmp=collector(None,data_num)
        self.collectors.append(tmp)
        self.actual_listselect=[0] 
        self.last_data=self.collectors[self.actual_listselect[0]].fast_get()
        self.track_data=[]
        self.initUI()
        
    def initUI(self):
        """initializes UI"""
        self.parent.title("Flight Recorder")          
        self.pack(fill=BOTH, expand=1)
        self.menubar()
        self.listbox()
        self.listbuttons()
        self.par_panel_names()
        self.par_panel()
        self.par_panel_units()
        self.par_panel_checkbuttons()
        self.par_panel_plot()
        
    def menubar(self):
        """initializes menubar"""
        menubar = Menu(self)
        misc = Menu(menubar, tearoff=0)
        misc.add_command(label="Archives",command=self.archives)
        misc.add_command(label="Exit",command=quit)
        menubar.add_cascade(label="Options", menu=misc)
        self.parent.config(menu=menubar)
    def listbox(self):
        """initializes listbox"""
        self.lb = Listbox(self)
        self.lb.grid(row=0,column=0,columnspan=2,rowspan=7)
        self.lb.bind("<<ListboxSelect>>", self.listselect)    
  
    def listbuttons(self):
        """initializes listbuttons"""
        addbtn=Button(self,text="Add",command=self.addcommand)
        addbtn.grid(row=7,column=0,sticky=N+W)
        delbtn=Button(self,text="Delete",command=self.delcommand)
        delbtn.grid(row=7,column=1,sticky=N+W)
    def par_panel_names(self):
        """initializes value names"""
        labels=[]
        for i in range(0,len(names)):
            labels.append(Label(self,text=names[i]))
            labels[i].grid(column=2,row=i)
            
    def par_panel(self):
        """initializes value place"""
        labels=[]
        self.values=[]
        for i in range(0,data_num):
            self.values.append('')
            self.values[i]=StringVar()
            labels.append(Label(self,text=0,textvariable=self.values[i]))
            labels[i].grid(column=3,row=i)
    
    def par_panel_units(self):
        """initializes value dimension"""
        labels=[]
        for i in range(0,len(units)):
            labels.append(Label(self,text=units[i]))
            labels[i].grid(column=4,row=i)
            
    def par_panel_checkbuttons(self):
        """initializes checkbuttons"""
        self.checkbuttons=[]
        self.checkbuttons_var=[]
        for i in range(0,data_num):
            self.checkbuttons_var.append('')
            self.checkbuttons_var[i]=IntVar()
            self.checkbuttons.append(Checkbutton(self,variable=self.checkbuttons_var[i],
            onvalue=1,offvalue=0,command=self.plot_manager))
            self.checkbuttons[i].grid(column=5,row=i)
            self.checkbuttons[i].deselect()
        self.checkbuttons_last=-1
        
    def par_panel_plot(self):
        """initializes plot"""
        f = Figure(figsize=(3, 2), dpi=100)
        self.canvas = FigureCanvasTkAgg(f, master=self)
        self.plot_handler = f.add_subplot(111)
        self.canvas.get_tk_widget().grid(column=6,row=0,rowspan=data_num+1)
        self.canvas.show()


    def listselect(self,val):
        """listselect function"""
        sender = val.widget
        self.actual_listselect = sender.curselection()
        del self.track_data[:]
    def addcommand(self):
        """addcommand function"""
        ftypes = [('Python files', '*.py'), ('All files', '*')]
        dlg = tkFileDialog.Open(self, filetypes = ftypes)
        fl = dlg.show()
        if fl != '':
            self.lb.insert(END,fl)
        self.lb.select_set(0)
        tmp=collector(fl,data_num)
        if self.collectors[0].path == None:
            self.collectors[0]=tmp
        else: 
            self.collectors.append(tmp)
    def delcommand(self):
        """delcommand function"""
        idx=self.lb.curselection()
        try:
            self.lb.delete(idx)
        except TclError:
            pass
        if not self.collectors:
            del self.collectors[idx[0]]
        if self.collectors:
            tmp=collector(None,data_num)
            self.collectors.append(tmp)
            self.actual_listselect=[0] 
            self.last_data=self.collectors[self.actual_listselect[0]].fast_get()
            
    def plot_manager(self):
        """plot_managing function"""
        del self.track_data[:]
        for i in range(0,data_num):
            if self.checkbuttons_last == -1:
                if self.checkbuttons_var[i].get() == 1:
                    self.checkbuttons_last=i
                    break
            else:
                self.checkbuttons[self.checkbuttons_last].deselect()
                self.checkbuttons_last=-1
                self.plot_manager()
                break
            
    def mainloopext(self):
        """extension for Tkinter mainloop"""
        try: 
            if self.actual_listselect:
                self.last_data=self.collectors[self.actual_listselect[0]].fast_get()  
            for i in range(0,data_num):
                self.values[i].set(str(self.last_data[i]))
                
            self.plot()
            self.auto_saving()
            self.parent.after(time_update,self.mainloopext)
        except IOError as e:
                tkMessageBox.showinfo("IOError",e.strerror+" or no input provided.")
        except ValueError:
                tkMessageBox.showinfo("ValueError","Incorrect file was chosen.")
    
    
    def plot(self):
        if self.checkbuttons_last != -1:
                if len(self.track_data) < 100000 :
                    self.track_data.append(self.last_data[self.checkbuttons_last])
                else:
                    self.track_data.pop(0)
                    self.track_data.append(self.last_data[0])
                
                self.plot_handler.clear()
                self.plot_handler.plot(self.track_data)
                tmp=self.track_data[len(self.track_data)-plot_cache:]
                self.plot_handler.axis([len(self.track_data)-plot_cache,len(self.track_data)-1, 
                min(tmp)-1,max(tmp)+1])
                self.canvas.show()
        else:
            del self.track_data[:]
    def auto_saving(self):
        if auto_save == 1:
                for el in self.collectors:
                    el.fast_get()
                    basic_save(el.path,el.last_data)
                
                
    def archives(self):
        """initalizes new archives GUI"""
        root_a=Tk()
        root_a.geometry("700x400+300+300")
        app_a = Arch_manager(root_a)
        self.parent.destroy()
        root_a.after(1,app_a.mainloopext)
        root_a.mainloop()
