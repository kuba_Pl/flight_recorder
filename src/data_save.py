
import pickle
import os
from config import *
def basic_save(flight_name,val_list):
    """saves val_list as pickle in user's defined folder with name flight_name"""
    if flight_name == None: return
    if not os.path.exists(save_path):
        os.makedirs(save_path)
    
    tmp=flight_name.split('/')
    name=tmp.pop()
    save=save_path+'/'+name
    with open(save, 'ab') as f:
        pickle.dump(val_list, f)
    f.close()
    return
