from ttk import *
from Tkinter import *
import tkFileDialog
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg


from config import *
from data_load import basic_load

class Arch_manager(Frame):
    """archives manager GUI class"""
    def __init__(self, parent):
        Frame.__init__(self, parent)  
        self.parent = parent
        self.data=[]
        self.sep_data=[]
        self.initUI()
    def initUI(self):
        self.parent.title("Archives manager")          
        self.pack(fill=BOTH, expand=1)
        self.menubar()
        self.par_panel_names()
        self.par_panel()
        self.par_panel_units()
        self.par_panel_entry()
        self.par_panel_checkbuttons()
        self.par_panel_plot()
        self.par_panel_scale()
    def menubar(self):
        """initializes menubar"""
        menubar = Menu(self)
        misc = Menu(menubar, tearoff=0)
        misc.add_command(label="Load file",command=self.load_file)
        misc.add_command(label="Exit",command=quit)
        menubar.add_cascade(label="Options", menu=misc)
        self.parent.config(menu=menubar)
   
    
    def par_panel_names(self):
        """initializes value names"""
        labels=[]
        for i in range(0,len(names)):
            labels.append(Label(self,text=names[i]))
            labels[i].grid(column=0,row=i)
        lab=Label(self,text="data number")
        lab.grid(column=0,row=data_num+1)
    def par_panel(self):
        """initializes value places"""
        self.var_labels=[]
        self.values_a=[]
        for i in range(0,data_num):
            self.values_a.append('')
            self.values_a[i]=StringVar()
            self.var_labels.append(Label(self,text=0))
            self.var_labels[i].grid(column=1,row=i)
            
    def par_panel_units(self):
        """initializes value dimensions"""
        labels=[]
        for i in range(0,len(units)):
            labels.append(Label(self,text=units[i]))
            labels[i].grid(column=2,row=i)
            
    def par_panel_checkbuttons(self):
        """initializes checkbottoms(due to problems with StringVar() class changed to sliders)"""
        self.checkbuttons=[]
        for i in range(0,data_num):
            self.checkbuttons.append(Scale(self,from_=0,to=1,
            command=self.plot_manager,orient=HORIZONTAL,length=50))
            self.checkbuttons[i].grid(column=3,row=i)
        self.checkbuttons_last=-1
    
    def par_panel_entry(self):
        """initializes entry panel"""
        self.entry_v=StringVar()
        self.entry=Entry(self,textvariable=self.entry_v)
        self.entry.insert(0,"0")
        self.entry.grid(column=1,row=data_num+1)
        
    def par_panel_plot(self):
        """initializes plot"""
        f = Figure(figsize=(3, 2), dpi=100)
        self.canvas = FigureCanvasTkAgg(f, master=self)
        self.plot_handler = f.add_subplot(111)
        self.canvas.get_tk_widget().grid(column=4,row=0,rowspan=data_num-2,sticky=S)
        self.canvas.show()
        
    def par_panel_scale(self):
        """initializes scale"""
        self.scale=Scale(self,from_=0,to=100,orient=HORIZONTAL,length=200)
        self.scale.grid(column=4,row=data_num-1,sticky=N)
        
    def load_file(self):
        """load file patch and pickles inside"""
        ftypes = [('Python files', '*.py'), ('All files', '*')]
        dlg = tkFileDialog.Open(self, filetypes = ftypes)
        fl = dlg.show()
        if fl != '':
            self.data=basic_load(fl)
        self.scale.config(to=len(self.data)-1)
        self.sep_data=[]
        for i in range(0,data_num):
            self.sep_data.append([])
            for el in self.data:
                self.sep_data[i].append(el[i])
                
    def plot_manager(self,widget):
        """plot managing method"""
        for i in range(0,data_num):
            if self.checkbuttons_last == -1:
                if self.checkbuttons[i].get() == 1:
                    self.checkbuttons_last=i
                    break
            else:
                self.checkbuttons[self.checkbuttons_last].set(0)
                self.checkbuttons_last=-1
                self.plot_manager(widget)
                break
    def mainloopext(self):
        """extension for mainloop()"""
        if self.data: 
            tmp=self.entry.get()
            try:
                num=int(tmp)
            except ValueError:
                num=0
            try:
                new_data=self.data[num]
            except IndexError:
                new_data=self.data[len(self.data)-1]
            for i in range(0,len(self.values_a)):
                self.values_a[i].set(new_data[i])
                self.var_labels[i].config(text=self.values_a[i].get())
        if self.checkbuttons_last != -1:
            if self.sep_data:
                mid=self.scale.get()
                self.plot_handler.clear()
                self.track_data=self.sep_data[self.checkbuttons_last]
                tmp=self.track_data[mid-int(plot_cache/2):mid+int(plot_cache/2)]
                if tmp:
                    self.plot_handler.plot(tmp)
                    self.canvas.show()
        self.parent.after(1,self.mainloopext)
   