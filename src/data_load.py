import pickle

def basic_load(path):
    """loads all pickles from path"""
    lists = []
    file = open(path, 'rb')
    while 1:
        try:
            lists.append(pickle.load(file))
        except EOFError:
            break
    file.close()
    return lists