#config for flight recorder 

data_num=7 #number of data input
#data input names
names=("altitude","latitude","roll","pitch","heading","airspeed","vertical speed") 
#data input units
units=("ft","deg","deg","deg","deg","kt","fps")
#tick count for flight recorder
time_update=1000
#number of points displayed on plots
plot_cache=30
#enable autosave
auto_save=1
#user's defined log's path
save_path="/home/kuba/Pulpit/pythonek/samoloty_1.0/logs"
# sign between values (data collector)
sep_sign=','